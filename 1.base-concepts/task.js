"use strict"

function solveEquation(a, b, c) {
	const arr = [];
	const D = Math.pow(b, 2) - 4 * a * c;

	if (D < 0) {
		return arr;
	}

	if (D == 0) {
		arr.push(-b / (2 * a));
	}

	if (D > 0) {
		arr.push(
			(-b + Math.sqrt(D)) / (2 * a)
		);
		arr.push(
			(-b - Math.sqrt(D)) / (2 * a)
		);
	}

	return arr;
}

function calculateTotalMortgage(percent, contribution, amount, countMonths) {
	const partPercent = (percent / 100) / 12;
	const loanBody = (amount - contribution);
	const monthlyFee = loanBody * (partPercent + (partPercent / ((1 + partPercent) ** countMonths - 1)));
	const totalMortgage = monthlyFee * countMonths;

	return +totalMortgage.toFixed(2);
}